import React from 'react';
import Listing from '../PropertyListings/Listing/Listing'
import './PropertyListings.css';

class PropertyListings extends React.Component {

constructor(props){
    super(props)
}

render() { 
    return <div className="listings">
    {this.props.listings.map(listing => (
      <Listing listing={listing} />
    ))}
  </div>
}

}

export default PropertyListings;