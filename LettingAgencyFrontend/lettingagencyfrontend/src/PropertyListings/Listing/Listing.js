import * as React from 'react';
import stockImage from '../Listing/image-coming-soon-placeholder.png';
import '../../PropertyListings/PropertyListings.css';

class Listing extends React.Component {

    constructor(props) {
        super(props)
        this.state = { isOpen: false };
    }

    toggleModal = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        return <div className="container">
            <div className="image">
                <img src={stockImage} />
            </div>
            <div className="details">
                <div><h2>{this.props.listing.type.type}</h2></div>
                <div><h3>{this.props.listing.description}</h3></div>
                <div><h4>&pound;{this.props.listing.price}ppm</h4></div>
                <div> Bedrooms: {this.props.listing.numBedroom}</div>
                <div> Bathrooms: {this.props.listing.numBathroom}</div>
                <div >{this.props.listing.location}</div>

                <div>
                    <button className="registerButton">
                        Register to find out more
                     </button>
                </div>
            </div>
        </div>
    }
}

export default Listing