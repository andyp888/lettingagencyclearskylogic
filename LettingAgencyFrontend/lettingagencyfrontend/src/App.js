import React from 'react';
import PropertyListings from '../src/PropertyListings/PropertyListings'
import './App.css';

class App extends React.Component {

  constructor() {
    super();
    this.state = {
      propertyListings: []
    }
  }

  componentDidMount() {
    fetch('https://localhost:5001/property/getproperties')
      .then(response => response.json())
      .then(data => this.setState({ propertyListings: data }));
  }

  render() {
    return <div className="App">
      <header className="App-header">
        <div className="bg-image-top">
          <div>
            <h1>Letting Agency</h1>
            <h4>Find your dream property today</h4>
          </div>
          <div className="accountButtons">
          <button className="registerButton">
            Register
                     </button>
          <button className="registerButton">
            Log In
                     </button>
        </div>
        </div>

      </header>
      <PropertyListings listings={this.state.propertyListings}>

      </PropertyListings>
    </div>
  }
}

export default App;
