﻿using LettingAgencyBackend.Model;
using LettingAgencyBackend.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LettingAgencyBackend.Mapper
{
    public static class UserMapper
    {
        public static User MapRequestToModel(CreateUserRequest req)
        {
            User result = new User();
            result.EmailAddress = req.EmailAddress;
            result.Name = req.Name;
            result.UserName = req.UserName;

            return result;
        }
    }
}
