﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LettingAgencyBackend.Requests
{
    public class CreateUserRequest
    {
        public string UserName { get; set; }
        public string Name { get; set; }
        public string EmailAddress { get; set; }
    }
}
