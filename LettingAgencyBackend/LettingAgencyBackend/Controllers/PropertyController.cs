﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LettingAgencyBackend.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace LettingAgencyBackend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PropertyController : ControllerBase
    {      
        private readonly ILogger<PropertyController> _logger;
        private readonly LettingAgencyDbContext _dbContext;

        public PropertyController(ILogger<PropertyController> logger, LettingAgencyDbContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
        }

        [HttpGet("GetProperties")]
        public async Task<ActionResult> GetProperties()
        {
            var x = _dbContext.Property.ToList();
            return Ok(x);
        }
    }
}
