﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LettingAgencyBackend.Mapper;
using LettingAgencyBackend.Requests;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace LettingAgencyBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly LettingAgencyDbContext _dbContext;

        public UserController(ILogger<UserController> logger, LettingAgencyDbContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
        }

        [HttpPost("CreateUser")]
        public async Task<ActionResult> CreateUser([FromBody]CreateUserRequest user)
        {
            try
            {
                var userModel = UserMapper.MapRequestToModel(user);
                _dbContext.User.Add(userModel);
                await _dbContext.SaveChangesAsync();
                return Ok(userModel);
            }

            catch(Exception e)
            {
                _logger.LogError(e.Message, e);
                return BadRequest();
            }
        }
    }
}