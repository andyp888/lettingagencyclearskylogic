﻿using LettingAgencyBackend.Model;
using System.Data.Entity;

namespace LettingAgencyBackend
{
    public class LettingAgencyDbContext : DbContext
    {
        public LettingAgencyDbContext(string connString) : base(connString)
        {
            Database.SetInitializer<LettingAgencyDbContext>(null);
        }

        public DbSet<User> User { get; set; }
        public DbSet<PropertyType> PropertyType { get; set; }
        public DbSet<Property> Property { get; set; }
    }
}
