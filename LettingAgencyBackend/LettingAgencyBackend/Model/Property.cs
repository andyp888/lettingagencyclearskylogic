﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LettingAgencyBackend.Model
{
    [Table("Property")]
    public class Property
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Type")]
        public int PropertyTypeId { get; set; }
        public virtual PropertyType Type { get; set; }
        public string Description { get; set; }
        public int NumBedroom { get; set; }
        public int NumBathroom { get; set; }
        public string Location { get; set; }
        public string Image { get; set; }
        public int Price { get; set; }
    }
}
