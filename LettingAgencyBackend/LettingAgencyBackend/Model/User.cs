﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace LettingAgencyBackend.Model
{
    [Table("User")]
    public class User
    {
        [Key]
        public int Id { get;set ; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string EmailAddress { get; set; }
    }
}
