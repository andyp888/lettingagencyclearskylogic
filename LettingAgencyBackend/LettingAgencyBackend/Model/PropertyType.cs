﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LettingAgencyBackend.Model
{
    [Table("PropertyType")]
    public class PropertyType
    {
        [Key]
        public int PropertyTypeId { get; set; }
        public string Type { get; set; }
    }
}
