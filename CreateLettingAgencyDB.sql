USE [master]
GO
/****** Object:  Database [LettingAgencyDB]    Script Date: 19/10/2020 20:18:20 ******/
CREATE DATABASE [LettingAgencyDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'LettingAgencyDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\LettingAgencyDB.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'LettingAgencyDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\LettingAgencyDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [LettingAgencyDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LettingAgencyDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LettingAgencyDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LettingAgencyDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LettingAgencyDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LettingAgencyDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LettingAgencyDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [LettingAgencyDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LettingAgencyDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LettingAgencyDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LettingAgencyDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LettingAgencyDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LettingAgencyDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LettingAgencyDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LettingAgencyDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LettingAgencyDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LettingAgencyDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LettingAgencyDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LettingAgencyDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LettingAgencyDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LettingAgencyDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LettingAgencyDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LettingAgencyDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LettingAgencyDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LettingAgencyDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [LettingAgencyDB] SET  MULTI_USER 
GO
ALTER DATABASE [LettingAgencyDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LettingAgencyDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LettingAgencyDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LettingAgencyDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [LettingAgencyDB] SET DELAYED_DURABILITY = DISABLED 
GO
USE [LettingAgencyDB]
GO
/****** Object:  Table [dbo].[Property]    Script Date: 19/10/2020 20:18:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PropertyTypeId] [int] NOT NULL,
	[Description] [varchar](250) NOT NULL,
	[NumBedroom] [int] NOT NULL,
	[NumBathroom] [int] NOT NULL,
	[Location] [varchar](250) NOT NULL,
	[Image] [image] NULL,
	[Price] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PropertyType]    Script Date: 19/10/2020 20:18:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PropertyType](
	[PropertyTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PropertyTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 19/10/2020 20:18:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Email Address] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Property] ON 

INSERT [dbo].[Property] ([Id], [PropertyTypeId], [Description], [NumBedroom], [NumBathroom], [Location], [Image], [Price]) VALUES (6, 1, N'Beautifully presented semi detached property', 3, 2, N'Edinburgh, United Kingdom', NULL, 600)
INSERT [dbo].[Property] ([Id], [PropertyTypeId], [Description], [NumBedroom], [NumBathroom], [Location], [Image], [Price]) VALUES (9, 3, N'Victorian Townhouse', 3, 2, N'Midlothian, United Kingdom', NULL, 700)
INSERT [dbo].[Property] ([Id], [PropertyTypeId], [Description], [NumBedroom], [NumBathroom], [Location], [Image], [Price]) VALUES (10, 2, N'Stunning modern flat', 4, 4, N'London, United Kingdom', NULL, 1250)
SET IDENTITY_INSERT [dbo].[Property] OFF
SET IDENTITY_INSERT [dbo].[PropertyType] ON 

INSERT [dbo].[PropertyType] ([PropertyTypeId], [Type]) VALUES (1, N'Semi-Detached')
INSERT [dbo].[PropertyType] ([PropertyTypeId], [Type]) VALUES (2, N'Flat')
INSERT [dbo].[PropertyType] ([PropertyTypeId], [Type]) VALUES (3, N'Detached')
SET IDENTITY_INSERT [dbo].[PropertyType] OFF
ALTER TABLE [dbo].[Property]  WITH CHECK ADD  CONSTRAINT [FK_Property_PropertyType] FOREIGN KEY([PropertyTypeId])
REFERENCES [dbo].[PropertyType] ([PropertyTypeId])
GO
ALTER TABLE [dbo].[Property] CHECK CONSTRAINT [FK_Property_PropertyType]
GO
USE [master]
GO
ALTER DATABASE [LettingAgencyDB] SET  READ_WRITE 
GO
